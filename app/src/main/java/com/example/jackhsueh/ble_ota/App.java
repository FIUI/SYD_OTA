package com.example.jackhsueh.ble_ota;

import android.app.Application;

import com.blankj.utilcode.util.Utils;
import com.clj.fastble.BleManager;

public class App extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        BleManager.getInstance().init(this);
        Utils.init(this);
    }
}
